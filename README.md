# FinalProject

MATH-458 course project of Monte Carlo simulation by Qing Jun & Ma Ningwei.

## Structure

```bash
.
├── CMakeLists.txt
├── Doxyfile
├── flow chart.png
├── html # Doxygen 
├── README.md
├── src # Source code
│   ├── Calculation.cpp
│   ├── Calculation.h
│   ├── CMakeLists.txt
│   ├── DimensionSampleGenerator.cpp
│   ├── DimensionSampleGenerator.h
│   ├── Exceptions.cpp
│   ├── Exceptions.h
│   ├── ext # External library code
│   │   ├── boost
│   │   ├── eigen
│   │   ├── googletest
│   │   └── nlohmann
│   ├── FileOutput.cpp
│   ├── FileOutput.h
│   ├── FileReader.cpp
│   ├── FileReader.h
│   ├── Func # Func class source code
│   │   ├── Exp.cpp
│   │   ├── Exp.h
│   │   ├── Parser.cpp
│   │   ├── Parser.h
│   │   ├── Token.cpp
│   │   └── Token.h
│   ├── Func.cpp
│   ├── Func.h
│   ├── input.json
│   ├── input.txt
│   ├── main.cpp
│   ├── SampleGenerator.cpp
│   ├── SampleGenerator.h
│   ├── test.json
│   ├── test.txt
│   ├── TheoryTest.cpp
│   └── TheoryTest.h
├── test # Google test suite code
│   └── test.cc

```

## How to compile the program
We use `CMake` to compile the system. To obtain `CMake` on Ubuntu/Debian simply call

```bash
sudo apt-get install cmake
```

To build the system what is required is a C++17-compatible compiler. For external dependencies such as boost math, json parser and googletest, we put light-weighted part of their source code inside source code `src/ext`.

Inside where this `README` stays, create a new directory to contain build artifacts, enter it, and then invoke `CMake` with the root project folder as argument as shown in the following example:
```bash
mkdir build

cd build

cmake ..
```
Afterwards, executable can be built and installed via

```bash
make -j
```
Then you could see `main`, `montecarlo_test` and other data/library needed inside this folder

## How to run the program

1. Inside your build folder, run the executable `main` in the command line in the terminal. The screen will instruct you. 
2. First part is to type in the file you want to read. The provided file are in `src/input.txt` and `src/input.json`. When you are required to type the File name, be sure the route is correct. If you input an incorrect filename, you will be given an error indicate either FileNotFound or json parse error. 
3. Second part is to choose the type of distribution you want to apply in the project. `Uniform`, `normal` distribution is provided, you can also use your own CDF function to generate the sample. The `func2` in the input file indicate it for each dimension.
4. Third part requires you to input an integer `m` indicating the order for calculating the statistical momentum of the function you just read. The `func1` in input file is the vectorial function map the vector sample to a scalar. You can try different order `m` until you want to break.
5. The fourth section uses the results of the momentum to verify central limit theory. It will use the function you read. the sample generator you have specified, a configurable `sample number` and generate an A-square statistic.  We should notice that if you use the self defined CDF function, the results might not near 0, since they might not be either independent or identically distributed.
6. The last part is to choose an output file to store the results, the order for calculating statistical momentum will only apply the one in the input file. If the file is not existing, the program will create a new one.

## Introduction to input files
In the files for input, there are some variables provided for generating samples, calculating statistical momentum.
1. Func1, this is a function expression for calculating statistical momentum. The variable can only be written as x1,x2,.... The variables can not exceed the dimension of sample. Note that for txt reader, the function expression should not contain space.
2. Func2, a vector of function expressions, each function represents a CDF function for generating samples in each dimension. 
3. Interval, a vector of pair data, each pair indicates left and right limits for each dimension for uniform or cdf function distribution. 
4. Sigma & Mu, two vectors contains sigma and mu for each dimension for generating uniform distributed sample.
5. Sample, int object, the number of samples you want to generate.
6. Dimension, int object, the dimension for generated sample, the dimension cannot exceed the length of the interval,mu,sigma,func2.


## Program flow
1. This program is made of 5 parts. We apply polymorphism in reading file, generating sample, and outputing. Besides the five main parts. A class Func is applied using string expression as function to map a high dimension input to scalar. The building of the function also uses polymorphism.
2. Besides polymorphism, we also apply template for FileReader.h, SampleGenerator.h, and DimensionSampleGenerator.h so that we can generate integer in the uniform distribution, but realising template requires user to specify in the code, so we only use double type in the flow chart.
3. For data structure, std::vector, std::vector<std::vector>, std::vector<std::pair>, std::map<std::string> are applied for reading from file, generating samples and outputing file. While in calculating we mainly use Eigen library to do work.
4. Here shows the flow chart of our programme.
    ![Tux, the Linux mascot](flow%20chart.png)


## List of features
1. The `FileReader` class can Read from txt or json file. In each file, the parameters and values are in dict-like shape which makes it easy for reader to understand and revise. A template is used for the interval to set integer boundary for some distribution.

2. The `SampleGenerator` class can generate random number following a specific distribution: (1) uniform random number within given interval; (2) normal random number with given $\mu$ and $\sigma$; (3) Given a user-defined CDF that is valid on a given interval, it can generate random number following the PDF of this CDF; Given the number of samples, it can also generate a specific sized batch of random number. The sample can be multi-dimensional with each dimension follows different distribution. It enables us to implement it in more complicated cases. Besides that, a template enables the uniform distribution to generate integers that satisfy the users.

3. The `Func` class constructs from a string and the vector size of input, parse the string and store the formula. If you want to evaluate this function with a number, you just need to pass parameter to the object like this`func(x) /* x is an Eigen::VectorXd*/`, and get the calculation result. It supports arithmetic (`+,-,*,/,^`), exponential(`exp`), trigonometric(`cos, sin, tan, atan`) and parentheses. The input variable should have the form `xn` where we support n from 1~30, which means we can accept a vector with no more than dimension 30. `Func` class uses shunting algorithm to parse the tokens and the detailed implementation is in Doxygen.

4. The `Calculation` class can calculate the statistical moment given a `(Func&, int m, Eigen::MatrixXd&)`, return a `double` which shows the `m` order moment of the `Func` given input `Eigen::MatrixXd&` (the column of the matrix is each variable, and the number of columns is the number of sample size). It can also receive a `(std::vector<double>&, int m)` and return the m order statistical moment of the given function over the given vector of values

5. The `TheoryTest` class can verify Central limit theory given a `DimensionSampleGenerator` pointer, a `Func` and a given number `N` for data points. For each data point, it will use the generator to generate a number (given sample number) of variables, calculate the average of the value evaluated by `Func`, and store it to a vector of `N`. Then we check if these `N` samples follow the normal distribution using `Anderson-Darling test` of boost math library.

6. The `FileOutput` class can output the results using json or txt format according to the choice of the user.


## List of tests
For test, we use `googletest` and the test suite code is in test/test.cc. Here is the explanation for them. You can run the executable `montecarlo_test` for sanity check


### Reading test
1. readfromtxt: We initialize a `TxtReader` and check if its output is what we expected
2. readfromjson: We initialize a `JsonReader` and check if its output is what we expected

### Function test
1. sqrt: We test if `Func` can parse `sqrt` and evaluate it
2. exp: We test if `Func` can parse `exp`, `log` and evaluate it
3. power: We test if `Func` can parse `^` and evaluate it
4. arithmetic (+,-,*,/): We test if `Func` can parse `+,-,*,/` and evaluate it
5. trigonometric (cos,sin,tan,atan): We test if `Func` can parse `sin,cos,tan,atan` and evaluate it
6. parentheses: We test if `Func` can parse `()` and evaluate the function in correct priority


### Random sample test
**NOTE:** Because we use C++ library to generate uniform random values, we assume that it must conform to a uniform random distribution. If needed we should use other method like chi square test to verify it.
1. normal: Verify if the samples generated satisfy normal distribution; We generate a batch of samples and use `Anderson-Darling test` to test it.


### Calculate moment test
1. Moment1order: We let the `Func` be an identity function, and use uniform sample from 0 to 1 to verify if the 1 order moment is 0.5
2. Moment2order: We let the `Func` be an identity function, and use uniform sample from 0 to 1 to verify if the 2 order moment is 1/3


### Central limit theory test
In our test, if A-square statistic (AD) computed by Anderson-Darling divided by total sample number is smaller than 1e-3, we will consider the tests to be passed. Because according to R.B. D'Augostino and M.A. Stephens, Eds., 1986, Goodness-of-Fit Techniques, Marcel Dekker, p-value of the fitting are given by:

```bash
Asq* = AD * (1 + 0.75/n + 2.25/n^2)

If Asq*=>0.6, then p = exp(1.2937 - 5.709(Asq*)+ 0.0186(Asq*)^2)

If 0.34 < Asq* < .6, then p = exp(0.9177 - 4.279(Asq*) - 1.38(Asq*)^2)

If 0.2 < Asq* < 0.34, then p = 1 - exp(-8.318 + 42.796(Asq*)- 59.938(Asq*)^2)

If Asq* <= 0.2, then p = 1 - exp(-13.436 + 101.14(Asq*)- 223.73(Asq*)^2)
```
We use n = 8192, and if Asq/n < 1e-3, we are almost 100% sure that the data is normally distributed

1. CentralLimitUsingNormalDistribution: We let the `Func` be an identity function, and use normal samples fed to it to verify
2. CentralLimitUsingUniformDistribution: We let the `Func` be an identity function, and use uniform samples fed to it to verify
3. CentralLimitUsingExponentialDistribution: We use a uniform sample $x_1$ and feed it into a `Func` with the format: $-log(1-x_1)$. From inverse method we know f(x) will have an exponential distribution, then we check if it also satisfies central limit theory test
4. CentralLimitUsingVectorialInput1: We use a 3D uniform sample $x_1, x_2, x_3$ and feed it into a `Func` with the format: $x_1 * x_3 + x_2 * (1-x_3)$. We use this to verify.
5. CentralLimitUsingVectorialInput2: We use a 2D uniform sample $x_1, x_2$ and feed it into a `Func` with the format: $cos(x_1) + sin(x_2) + exp(x_1 + x_2)$. We use this to verify.
