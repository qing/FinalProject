cmake_minimum_required(VERSION 3.0)
project(FinalProject)
set(CMAKE_CXX_STANDARD 17)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
#target_link_libraries(FileReader.h ${CURL_LIBRARIES})
include_directories(eigen)
include_directories(exprtk)
include_directories(json)
include_directories(nlohmann)
add_executable(run main.cpp Exceptions.cpp FileReader.cpp Calculation.hpp/
        FileReader.h  Func.h SampleGenerator.h Exceptions.h TheoryTest.h FileOutput.h SampleGenerator.cpp Func.cpp)
#Exceptions.cpp FileReader.cpp
file(COPY input.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
file(COPY input.json DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# find_package(nlohmann_json 3.7.0 REQUIRED)
#target_link_libraries(configuration PRIVATE nlohmann_json::nlohmann_json)
