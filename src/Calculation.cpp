#include "Calculation.h"


Calculation::Calculation() = default;
Calculation::~Calculation() = default;

double Calculation::CalMoment(Func& func, int m, Eigen::MatrixXd vars){
    double moment = 0;
    for(int i = 0; i<vars.cols(); i++){
        moment = moment + pow(func(vars.col(i)), m);
    }
    return moment / vars.cols();
}

double Calculation::CalMoment(std::vector<double>& vars, int m){
    double moment = 0;
    for(int i = 0; i<vars.size(); i++){
        moment = moment + pow(vars[i], m);
    }
    return moment / vars.size();
}
