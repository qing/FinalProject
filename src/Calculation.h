//
// Created by xiaoqing on 22-11-26.
//

#pragma once
#ifndef FINALPROJECT_CALCULATION_H
#define FINALPROJECT_CALCULATION_H
#include <eigen/Eigen/Dense>
#include <Func.h>
#include <SampleGenerator.h>
#include "Exceptions.h"

/**
 * a class for calculating the moment of a set of data or a function given a set of variables
 */
class Calculation {
public:
    Calculation();
    ~Calculation();

    /**
     * Calculate the moment of any function and a given set of input variable
     *
     * \param[in] func
     *     The user defined function
     * \param[in] m
     *     Moment order
     * \param[in] x
     *     Input random variable
     * \return
     *     The m order statistical moment of the given function over x
     */
    double CalMoment(Func& func, int m, Eigen::MatrixXd x);

    /**
     * Calculate the moment of any function and a given set of input variable
     *
     * \param[in] m
     *     Moment order
     * \param[in] x
     *     Input random function used to calculate moment
     * \return
     *     The m order statistical moment of the given function over x
     */
    double CalMoment(std::vector<double>& x, int m);

};

#endif //FINALPROJECT_CALCULATION_H
