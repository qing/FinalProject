//
// Created by xiaoqing on 22-12-7.
//
#include "DimensionSampleGenerator.h"

template<typename T>
DimensionSampleGenerator<T>::DimensionSampleGenerator() = default;
template<typename T>
DimensionSampleGenerator<T>::~DimensionSampleGenerator() = default;

template <typename T>
NormalDimensionSampleGenerator<T>::NormalDimensionSampleGenerator\
(int dimension, std::vector<T> &mu, std::vector<T> &sigma, int sample_num) \
: dimension(dimension),mu(mu),sigma(sigma),sample(sample_num){}

template <typename T>
GeneralDimensionGenerator<T>::GeneralDimensionGenerator\
(int dimension, std::vector<std::pair<T,T>> &interval, int sample_num,std::vector<std::string> &funcs)\
: dimension(dimension),interval(interval),sample(sample_num),funcs(funcs){}

template <typename T>
UniformDimensionGenerator<T>::UniformDimensionGenerator\
(int dimension, std::vector<std::pair<T,T>> &interval, int sample_num)\
: dimension(dimension),interval(interval),sample(sample_num){}

template <typename T>
Eigen::MatrixXd NormalDimensionSampleGenerator<T>::getDimensionSample() {
    if(this->sigma.size()!=this->mu.size())  throw DimensionNotMatchException("Dimension not matched");
    if(this->sigma.size() > this->dimension)  throw DimensionNotMatchException("Dimension not matched");
    Eigen::MatrixXd samplevector(this->dimension,this->sample);
    for(int i=0;i<this->dimension;i++){
        NormalGenerator<T> generator(this->mu[i],this->sigma[i]);
        for(int j=0;j<sample;j++){
            samplevector(i,j) = generator();
        }
    }
    return samplevector;
}

template <typename T>
Eigen::MatrixXd UniformDimensionGenerator<T>::getDimensionSample() {
    if(this->interval.size() > this->dimension)  throw DimensionNotMatchException("Dimension not matched");
    Eigen::MatrixXd samplevector(this->dimension,this->sample);
    for(int i=0;i<this->dimension;i++){
        UniformGenerator<T> generator(this->interval[i]);
        for(int j=0;j<sample;j++){
            samplevector(i,j) = generator();
        }
    }
    return samplevector;
}

template <typename T>
Eigen::MatrixXd GeneralDimensionGenerator<T>::getDimensionSample() {
    if (this->interval.size() > this->dimension) throw DimensionNotMatchException("Dimension not matched");
    Eigen::MatrixXd samplevector(this->dimension,this->sample);
    for (int i = 0; i < this->dimension; i++) {
        auto functor = this->funcs[i];
        GeneralSampelGenerator<T> generator(functor, this->interval[i].first, this->interval[i].second, 1000);
        for (int j = 0; j < sample; j++) {
            samplevector(i, j) = generator();
        }
    }
    return samplevector;
}