//
// Created by xiaoqing on 22-12-7.
//
#pragma once
#ifndef FINALPROJECT_DIMENSIONSAMPLEGENERATOR_H
#define FINALPROJECT_DIMENSIONSAMPLEGENERATOR_H
#include "SampleGenerator.h"
#include "Exceptions.h"
#include "eigen/Eigen/Dense"
/**
 * parent class for generating multi dimensional samples
 */
template<typename T>
class DimensionSampleGenerator{
public:
    DimensionSampleGenerator();
    ~DimensionSampleGenerator();
    virtual Eigen::MatrixXd getDimensionSample() = 0;
};
/**
 * generating multi-dimensional normal distribution
 */
template<typename T>
class NormalDimensionSampleGenerator:public DimensionSampleGenerator<T>{
public:
    NormalDimensionSampleGenerator(int dimension, std::vector<T> &mu, std::vector<T> &sigma, int sample_num);
    Eigen::MatrixXd getDimensionSample();
private:
protected:
    int sample;
    int dimension;
    std::vector<T> mu;
    std::vector<T> sigma;
};
/**
 * generating multi-dimensional uniform distribution with each dimension independent
 */
template <typename T>
class UniformDimensionGenerator : public DimensionSampleGenerator<T>{
public:
    explicit UniformDimensionGenerator(int dimension, std::vector<std::pair<T,T>> &interval, int sample_num);
    Eigen::MatrixXd getDimensionSample();
private:
protected:
    int sample;
    int dimension;
    std::vector<std::pair<T,T>> interval;

};
/**
 * generating multi-dimensional distribution with each dimension follow a specific cdf function
 */
template <typename T>
class GeneralDimensionGenerator : public DimensionSampleGenerator<T>{
public:
    explicit GeneralDimensionGenerator(int dimension, std::vector<std::pair<T,T>> &interval, int sample_num, std::vector<std::string> &funcs);
    Eigen::MatrixXd getDimensionSample();
private:
protected:
    int sample;
    int dimension;
    std::vector<std::pair<T,T>> interval;
    std::vector<std::string> funcs;

};
#endif //FINALPROJECT_DIMENSIONSAMPLEGENERATOR_H
