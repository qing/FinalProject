//
// Created by xiaoqing on 22-11-27.
//
#include "Exceptions.h"
#include <iostream>
#include <utility>


Exceptions::Exceptions(std::string tagString, std::string probString)
        : mTag(std::move(tagString)), mProblem(std::move(probString)) {}

void Exceptions::PrintDebug() const {
    std::cerr << "** Error (" << mTag << ") **\n";
    std::cerr << "Problem: " << mProblem << "\n\n";
}

FileNotOpenException::FileNotOpenException(std::string probString)
        : Exceptions("FILE_NOT_OPEN", std::move(probString)) {}

InvalidInterval::InvalidInterval(std::string probString)
        : Exceptions("INVALID_INTERVAL", std::move(probString)) {}

InvalidFunctionException::InvalidFunctionException(std::string probString)
        : Exceptions("INVALID_FUNCTION", std::move(probString)) {}

RuntimeException::RuntimeException(std::string probString)
        : Exceptions("RUNTIME_ERROR", std::move(probString)) {}

EmptyVariableException::EmptyVariableException(std::string probString)
        : Exceptions("EMPTY VARIABLE", std::move(probString)) {}

DimensionNotMatchException::DimensionNotMatchException(std::string probString)
        : Exceptions("DIMENSION NOT MATCH", std::move(probString)) {}

CDFNotMonotonic::CDFNotMonotonic(std::string probString)
: Exceptions("CDF Not Monotonic", std::move(probString)) {}


FailToEvaluateException::FailToEvaluateException(std::string probString): 
Exceptions("FAILED TO EVALUATE", std::move(probString)) {}


FailToParseException::FailToParseException(std::string probString): 
Exceptions("FAILED TO PARSE", std::move(probString)) {}


ReadTokenException::ReadTokenException(std::string probString): 
Exceptions("FAILED TO READ FUNCTION TOKENS", std::move(probString)) {}
