//
// Created by xiaoqing on 22-11-26.
//

#ifndef FINALPROJECT_EXCEPTIONS_H
#define FINALPROJECT_EXCEPTIONS_H
#include <iostream>

/**
 * Exception class for reminding the error type
 */
class Exceptions {
private:
    std::string mTag, mProblem;

public:
    Exceptions(std::string tagString, std::string problemString);
    void PrintDebug() const;
};

/**
 * Exception class for files not found error
 */
class FileNotOpenException : public Exceptions {
public:
    explicit FileNotOpenException(std::string probString);
};

/**
 * Exception class for boundary not legal error
 */
class InvalidInterval : public Exceptions {
public:
    explicit InvalidInterval(std::string probString);
};

/**
 * Exception class for function not legal
 */
class InvalidFunctionException : public Exceptions {
public:
    explicit InvalidFunctionException(std::string probString);
};

/**
 * Exception class for run time to long error
 */
class RuntimeException : public Exceptions {
public:
    explicit RuntimeException(std::string probString);
};

/**
 * Exception class for empty variable error
 */
class EmptyVariableException : public Exceptions {
public:
    explicit EmptyVariableException(std::string probString);
};

/**
 * Exception class for dimension not match
 */
class DimensionNotMatchException : public Exceptions {
public:
    explicit DimensionNotMatchException(std::string probString);
};

class CDFNotMonotonic: public Exceptions {
public:
    explicit CDFNotMonotonic(std::string probString);
};


/**
 * Exception class for failing to evaluate
 */
class FailToEvaluateException : public Exceptions {
public:
    explicit FailToEvaluateException(std::string probString);
};

/**
 * Exception class for failing to parse
 */
class FailToParseException : public Exceptions {
public:
    explicit FailToParseException(std::string probString);
};


/**
 * Exception class for failing to read tokens
 */
class ReadTokenException : public Exceptions {
public:
    explicit ReadTokenException(std::string probString);
};



#endif //FINALPROJECT_EXCEPTIONS_H
