//
// Created by xiaoqing on 22-12-8.
//
#include "FileOutput.h"

FileOutput::FileOutput() = default;
FileOutput::~FileOutput() = default;

JsonOutput::JsonOutput\
(double similarity,int m, double statistical_m,std::vector<std::string> &distribution_type,std::string &func_m)\
: similarity(similarity),m(m),statistical_m(statistical_m),distribution_type(distribution_type),func_m(func_m){}

TxtOutput::TxtOutput\
(double similarity,int m, double statistical_m,std::vector<std::string> &distribution_type,std::string &func_m)\
:similarity(similarity),m(m),statistical_m(statistical_m),distribution_type(distribution_type),func_m(func_m){}


void JsonOutput::outPut(std::string linker){
    nlohmann::json jsonfile;
    std::ofstream file;
    file.open(linker);
    jsonfile["similarity to follow normal distribution"] = this->similarity;
    jsonfile["distribution type"] = this->distribution_type;
    jsonfile["function for statistical momentum"] = this->func_m;
    jsonfile["order for statistical momentum "] = this->m;
    jsonfile["results for statistical momentum"] = this->statistical_m;
    file<<jsonfile;
    file.flush();
    file.close();
}

void TxtOutput::outPut(std::string linker) {
    std::ofstream file;
    file.open (linker);
    file<< "similarity to follow normal distribution  " << this->similarity<<"\n";
    file<< "distribution type  "<<"\n";
    for(auto it : distribution_type) file<<it<<" ";
    file<<"\n";
    file<< "function for statistical momentum  " << this->func_m<<"\n";
    file<< "order for statistical momentum  " << this->m<<"\n";
    file<< "results for statistical momentum  " << this->statistical_m<<"\n";
    file.flush();
    file.close();
}
