//
// Created by xiaoqing on 22-11-26.
//

#pragma once
#ifndef FINALPROJECT_FILEOUTPUT_H
#define FINALPROJECT_FILEOUTPUT_H

#include <iostream>
#include <vector>
#include "fstream"
#include "nlohmann/json.hpp"
/**
 * parent class for output result to file
 */
class FileOutput{
public:
    FileOutput();
    ~FileOutput();
    virtual void outPut(std::string  linker)=0;

};

/**
 * this class is used for output contents to json file
 */
class JsonOutput:public FileOutput{
public:
    JsonOutput(double similarity,int m, double statistical_m,std::vector<std::string> &distribution_type,std::string &func_m);
    void outPut(std::string  linker) override;
private:
    double similarity;
    int m;
    double statistical_m;
    std::vector<std::string> distribution_type;
    std::string func_m;
};

/**
 * this class is used for output contents to txt file
 */
class TxtOutput:public FileOutput{
public:
    TxtOutput(double similarity, int m, double statistical_m,std::vector<std::string> &distribution_type,std::string &func_m);
    void outPut(std::string  linker) override;
private:
    double similarity;
    int m;
    double statistical_m;
    std::vector<std::string> distribution_type;
    std::string func_m;
};
#endif //FINALPROJECT_FILEOUTPUT_H
