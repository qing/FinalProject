//
// Created by xiaoqing on 22-11-28.
//
#include "FileReader.h"
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <map>
#include <iterator>

template <class T>
FileReader<T>::FileReader()= default;;
template <class T>
FileReader<T>::~FileReader()= default;;

template <class T>
TxtReader<T>::TxtReader(const std::string &url){
    this->link = url;
};;
template <class T>
JsonReader<T>::JsonReader(const std::string &url){
    this->link = url;
};;

/**
 * implement the function to read data from txt file
 */
template <class T>
void TxtReader<T>::readData() {

    /// check if url is valid
    std::ifstream paramFile(this->link);
    if(paramFile.fail()) throw FileNotOpenException("could not open the file");

    /// read the value and put it to map
    std::string line;
    std::string key;
    std::string value;
    while ( paramFile >> key >> value ) {
        this->reader[key] = value;
    }

    /// assign value
    for (auto & param : this->reader)
    {
        if(param.first=="func1")  this->func_pdf = param.second;  // string (key)
        else if(param.first=="func2"){
            std::string tmp;
            std::stringstream ss(param.second);
            while(getline(ss, tmp, ',')){
                this->func_m.push_back(tmp);
            }
        }
        else if(param.first=="interval"){
            std::string tmp;
            std::stringstream ss(param.second);
            std::vector<T> temp;
            while(getline(ss, tmp, ',')){
                std::istringstream sst(tmp);
                T st; sst>>st;
                temp.push_back(st); ///TODO : transfer value from string
            }
            auto it = temp.begin();
            for (it; it != temp.end(); it += 2) {
                if(*it>=*(it+1)) throw InvalidInterval("Intervals not valid");
                std::pair<T, T> pair(*it, *(it + 1));
                this->interval.push_back(pair);}
        }
        else if(param.first=="mu"){
            std::string tmp;
            std::stringstream ss(param.second);
            while(getline(ss, tmp, ',')){
                std::istringstream sst(tmp);
                T st; sst>>st;
                this->mu.push_back(st);
            }
        }
        else if(param.first=="sigma"){
            std::string tmp;
            std::stringstream ss(param.second);
            while(getline(ss, tmp, ',')){
                std::istringstream sst(tmp);
                T st; sst>>st;
                this->sigma.push_back(st);
            }
        }
        else if(param.first=="sample"){
            this->n = std::stoi(param.second);
        }
        else if(param.first=="dimension"){
            this->dimension = std::stoi(param.second);
        }
        else if(param.first=="m"){
            this->m = std::stoi(param.second);
        }
        else std::cout<<"Warnings: contain undetected index "<<param.first<<std::endl;
    }

}

template <class T>
void JsonReader<T>::readData() {
    /// read the json file, really efficient and simple
    std::ifstream f(this->link);
    nlohmann::json data = nlohmann::json::parse(f);
//    std::cout << data;
//    std::cout<<std::endl;
    /// assign the data
    this->func_pdf = data["func1"];
    for (auto func:data["func2"]) this->func_m.push_back(func);
    this->interval = data["interval"].get<std::vector<std::pair<T,T>>>();
    this->sigma = data["mu"].get<std::vector<T>>();
    this->mu = data["mu"].get<std::vector<T>>();
    this->m = data["m"];
    this->n = data["sample"];
    this->dimension = data["dimension"];
    /// check if interval is valid
    auto it  = this->interval.begin();

    for(it;it!=this->interval.end();it++){
        if(it->first>=it->second)
            throw InvalidInterval("Intervals not valid");
    }


}


/**
 * implement the function to get the value inside the class
 */
template <class T>
std::string FileReader<T>::getFuncPdf() {
    return func_pdf;
}
template <class T>
std::vector<std::string> FileReader<T>::getFunc() {
    return func_m;
}
template <class T>
std::vector<std::pair<T,T>> FileReader<T>::geInterval() {
    return interval;
}
template <class T>
int FileReader<T>::getOrder() {
    return m;
}
template <class T>
int FileReader<T>::getSampleNumber() {
    return n;
}
template <class T>
std::vector<T> FileReader<T>::getMu(){
    return mu;
}

template <class T>
std::vector<T> FileReader<T>::getSigma(){
    return sigma;
}

template <class T>
int FileReader<T>::getDimension(){
    return dimension;
}


