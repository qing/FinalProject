//
// Created by xiaoqing on 22-11-26.
//
#pragma once
#ifndef FINALPROJECT_FILEREADER_H
#define FINALPROJECT_FILEREADER_H

#include <iostream>
#include <map>
#include <vector>
#include "Exceptions.h"
#include "nlohmann/json.hpp"
#include <fstream>

/**
 * this class is used for reading contents from different source
 */
template <class T>
class FileReader{
public:
    /// constructor
    FileReader();

    /// Release memory
    ~FileReader();

    /// Read and assign value
    virtual void readData()=0;

    /// get the value read by the constructor
    std::string getFuncPdf();  /// transfer string to lambda function
    std::vector<std::string> getFunc();
    std::vector<std::pair<T,T>> geInterval();
    std::vector<T> getMu();
    std::vector<T> getSigma();
    int getDimension();
    int getSampleNumber();
    int getOrder();

protected:
    /// the html link or the link to csv data file
    std::string link;
    std::map <std::string, std::string> reader; ///reading params
    std::string func_pdf; /// the string version of the pdf function
    std::vector<std::string> func_m; /// the string version function for calculating statistical moments
    int m; ///order for calculating statistical moments
    int n; /// sampling number
    int dimension;
    std::vector<std::pair<T,T>> interval; /// intervals for sampling
    std::vector<T> mu;
    std::vector<T> sigma;

};


/**
 * this class is used for reading contents from txt source
 */
template <class T>
class TxtReader: public FileReader<T>{
public:
    explicit TxtReader(const std::string &url);
    void readData();
};

/**
 * this class is used for reading contents from html source
 */
template <class T>
class JsonReader: public FileReader<T>{
public:
    explicit JsonReader(const std::string &url);
    void readData();
};

#endif //FINALPROJECT_FILEREADER_H
