//
// Created by xiaoqing on 22-12-2.
//
#include "Func.h"

Func::Func(int dimin, const std::string& expr): dimin(dimin) {
    Parser parser(expr, dimin);
    ys = std::unique_ptr<Expr>(parser.parse());
}


double Func::operator()(Eigen::Matrix<double, Eigen::Dynamic, 1> x){
    double y = 0;
    y = ys->evaluate(x);
    return y;
}

int Func::getSize(){
    return dimin;
}