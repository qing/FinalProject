#ifndef FINALPROJECT_FUNC_H
#define FINALPROJECT_FUNC_H

#include "eigen/Eigen/Dense"
#include "Func/Parser.h"
#include "memory"

#pragma once

/**
 * a class for calculating the results with an input string indicating the function, maps n dimension to 1 dimension
 */
class Func {
public:
    Func(int dimin, const std::string& expr);
    double operator()(Eigen::Matrix<double, Eigen::Dynamic, 1> x);
    int getSize();

private:
    int dimin;
    std::unique_ptr<Expr> ys;
};

#endif