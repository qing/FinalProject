#include "Func/Exp.h"

Expr::Expr(){
    
}

Expr::~Expr(){

}


Arith::Arith(TokenType opType, Expr* left, Expr* right): opType(opType), leftExpr(left), rightExpr(right){
    op = opType;
    isUnary = false;
};

Arith::~Arith(){
    if (leftExpr){
        delete leftExpr;
    }
    if (rightExpr){
        delete rightExpr;
    }
}

double Arith::evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& parameters){
    // addition case
    if (opType == TokenType::ADD){
        return leftExpr->evaluate(parameters) + rightExpr->evaluate(parameters);
    }
    //subtraction case
    if (opType == TokenType::SUB){
        return leftExpr->evaluate(parameters) - rightExpr->evaluate(parameters);
    }
    // multiplication case
    if (opType == TokenType::MUL){
        return leftExpr->evaluate(parameters) * rightExpr->evaluate(parameters);
    }
    // division case
    if (opType == TokenType::DIV){
        // verify division by 0
        if (rightExpr->evaluate(parameters) == 0){
            throw FailToEvaluateException("Error divide by zero");
        }
        return leftExpr->evaluate(parameters) / rightExpr->evaluate(parameters);
    }
    // power case
    if (opType == TokenType::POW){
        return pow(leftExpr->evaluate(parameters), rightExpr->evaluate(parameters));
    }
    throw FailToEvaluateException("OperatorNode Invalid operation type");    
}

Function::Function(FunctionType ftype, Expr* param): ftype(ftype), expr(param) {
    op = FUNCTION;
    isUnary = true;
}

Function::~Function(){
    if (expr){
        delete expr;
    }
}

double Function::evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& parameters){
    if (ftype == FunctionType::SIN) {
        return sin(expr->evaluate(parameters));
    }
    if (ftype == FunctionType::COS) {
        return cos(expr->evaluate(parameters));
    }
    if (ftype == FunctionType::TAN) {
        return tan(expr->evaluate(parameters));
    }
    if (ftype == FunctionType::ATAN) {
        return atan(expr->evaluate(parameters));
    }
    if (ftype == FunctionType::SQRT) {
        if(expr->evaluate(parameters) < 0){
            throw FailToEvaluateException("error sqrt of negative number");
        }
        return sqrt(expr->evaluate(parameters));
    }
    if (ftype == FunctionType::EXP){
        return exp(expr->evaluate(parameters));
    }
    if (ftype == FunctionType::LOG){
        if(expr->evaluate(parameters) < 0){
            throw FailToEvaluateException("error log of negative number");
        }
        return log(expr->evaluate(parameters));
    }
    throw FailToEvaluateException("Function is not yet implemented");   
}

Num::Num(double v): value(v){
    op = NUMBER;
    isUnary = true;
}

double Num::evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& x){
    return value;
}

Variable::Variable(VarType vtype): vtype(vtype) {
    op = VARIABLE;
    isUnary = true;
}

double Variable::evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& x){
    int id = vtype-1;
    return x(id);
}