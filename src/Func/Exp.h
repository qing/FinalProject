#pragma once

#include "Token.h"
#include "Exceptions.h"
#include "eigen/Eigen/Dense"


/**
 * The base class for store the math expression and evaluate it
 */
class Expr{
public:
    Expr();
    virtual ~Expr();

    /**
     * Calculate the moment of any function and a given set of input variable
     *
     * \param[in] x
     *     The numerical value of the varibale
     * \return
     *     The numerical results of the function
     */
    virtual double evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& x) = 0;

protected:
    TokenType op;
    bool isUnary;

};

/**
 * The class for store the arith binary expression and evaluate it
 */
class Arith: public Expr{
public:
    Arith(TokenType op, Expr* left, Expr* right);
    ~Arith();
    virtual double evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& x) override;

private:
    TokenType opType;
    Expr* leftExpr;
    Expr* rightExpr;
};


/**
 * The class for store the function expression and evaluate it
 */
class Function: public Expr{
public:
    Function(FunctionType ftype, Expr*);
    ~Function();
    virtual double evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& x) override;
private:
    FunctionType ftype;
    Expr* expr;
};

/**
 * The class for store the number expression and evaluate it
 */
class Num: public Expr{
public:
    Num(double v);
    virtual double evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& x) override;

private:
    double value;
};

/**
 * The class for store the variable name and evaluate it based on input value
 */
class Variable: public Expr{
public:
    Variable(VarType vtype);
    virtual double evaluate(const Eigen::Matrix<double, Eigen::Dynamic, 1>& x) override;

private:
    VarType vtype;
};