#include "Parser.h"

Parser::Parser(std::string str, int k): tokenStream(str, k){

}

Parser::~Parser(){

}

void Parser::reset(std::string str, int k){
    tokenStream.reset(str, k);
    if (expr){
        delete expr;
    }
    nums.clear();
}

Expr* Parser::parse(){
    if (expr){
        std::cout<<"Old expression will be deleted"<<std::endl;
        nums.clear();
        delete expr;
    }
    tokenStream.shunting(nums);

    std::stack<Expr*> st;
    Expr* node = nullptr;

    for(int i = 0; i<nums.size(); i++){
        node = nullptr;
        if (nums[i].getType() == NUMBER){
            node = new Num(nums[i].getValue());
            st.push(node);
        }
        else if (nums[i].getType() == VARIABLE){
            node = new Variable(nums[i].getVar());
            st.push(node);
        }
        // Unary
        else if (nums[i].getType() == FUNCTION){
            if (st.empty()){
                throw FailToParseException("Cannot build legal expression!");
            }
            node = new Function(nums[i].getFunc(), st.top());
            st.pop();
            st.push(node);
        }
        // Binary
        else if (nums[i].getType() == ADD || nums[i].getType() == SUB || nums[i].getType() == DIV || nums[i].getType() == MUL || nums[i].getType() == POW){
            if (st.empty()){
                throw FailToParseException("Cannot build legal expression!");
            }
            Expr* right = st.top();
            st.pop();
            if (st.empty()){
                throw FailToParseException("Cannot build legal expression!");
            }
            node = new Arith(nums[i].getType(), st.top(), right);
            
            st.pop();
            st.push(node);
        }

    }

    if (st.size() != 1){
        throw FailToParseException("Cannot build legal expression!");
    }
    return st.top();
}

