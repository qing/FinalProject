#pragma once
#include "Token.h"
#include "Exp.h"
#include "stack"
#include "Exceptions.h"


/**
 * a class for parsing the string to a math expression
 */
class Parser{
public:
    Parser(std::string str, int k);
    ~Parser();
    /**
     * Reset the exprssion and parse a new string
     *
     * \param[in] str
     *     The user defined function string
     * \param[in] k
     *     Dimension of the new function
     */
    void reset(std::string str, int k);
    /**
     * Parse the expresssion stored
     */
    Expr* parse();


private:
    std::vector<Token> nums; ///< stack for parse reverse polish notation
    TokenStream tokenStream; ///< class for reading the tokens
    Expr* expr = nullptr; ///< generated exprssion class after parse()
};