#include "Token.h"
#include <ctype.h>
#include <stack>


Token::Token(){ }
Token::Token(TokenType t): type(t), isSet(true){ }
Token::Token(TokenType t, char a): type(t), op(a), isSet(true) {str.push_back(a);}
Token::Token(TokenType t, FunctionType f, std::string& word): type(t), func(f), str(word), isSet(true) { }
Token::Token(TokenType t, VarType v, std::string& word): type(t), var(v), str(word), isSet(true) { }
Token::Token(TokenType t, double v, std::string word): type(t), value(v), str(word), isSet(true) { }

TokenType Token::getType() const{
    return type;
}

double Token::getValue() const{
    return value;
}

VarType Token::getVar() const{
    return var;
}

FunctionType Token::getFunc() const{
    return func;
}

std::ostream &operator<<( std::ostream &output, const Token &D){
    output<<D.str;
    return output;
}


std::unordered_set<char> TokenStream::BLANK = {' ', '\r', '\n', '\f'};\
std::unordered_map<std::string, FunctionType> TokenStream::Functions = {
    {"exp", EXP},
    {"sum", SUM},
    {"sqrt", SQRT},
    {"log", LOG},
    {"cos", COS},
    {"sin", SIN},
    {"tan", TAN},
    {"atan", ATAN},   
};
std::unordered_map<std::string, VarType> TokenStream::Vars{
    {"x1", x1},
    {"x2", x2},
    {"x3", x3},
    {"x4", x4},
    {"x5", x5},
    {"x6", x6},
    {"x7", x7},
    {"x8", x8},
    {"x9", x9},
    {"x10", x10},
    {"x11", x11},
    {"x12", x12},
    {"x13", x13},
    {"x14", x14},
    {"x15", x15},
    {"x16", x16},
    {"x17", x17},
    {"x18", x18},
    {"x19", x19},
    {"x20", x20},
    {"x21", x21},
    {"x22", x22},
    {"x23", x23},
    {"x24", x24},
    {"x25", x25},
    {"x26", x26},
    {"x27", x27},
    {"x28", x28},
    {"x29", x29},
    {"x30", x30},
};

Token TokenStream::next() {
    read();

    if (index >= int(curr.size())){
        isEnd = true;
        return Token(TokenType::END);
    }

    // blank
    if (TokenStream::BLANK.count(peek)) {
        return next();
    }

    Token token;

    // number
    if (isdigit(peek)) {
        readNumber(token);
    }

    // letter
    else if (isalpha(peek)) {
        readWord(token);
    }

    // operator
    else if (peek == '+') {
        token = Token(TokenType::ADD, peek);
    }
    else if (peek == '-') {
        token = Token(TokenType::SUB, peek);
    }
    else if (peek == '*') {
        token = Token(TokenType::MUL, peek);
    }
    else if (peek == '/') {
        token = Token(TokenType::DIV, peek);
    }
    else if (peek == '^') {
        token = Token(TokenType::POW, peek);
    }

    // dilimiter
    else if (peek == '(') {
        token = Token(TokenType::OPEN_PAR, peek);
    }
    else if (peek == ')') {
        token = Token(TokenType::CLOSE_PAR, peek);
    }

    if (token.isSet == false) {
        // TODO EXCEPTION
        throw ReadTokenException("Failed to set token!\n");
    }

    peek = '\0';
    return token;
}


TokenStream::TokenStream(std::string& input, int k): curr(input), dim(k){
    #ifdef DEBUG
    std::cout<<"Parsing "<<curr<<std::endl;
    #endif
}

void TokenStream::reset(std::string& input, int k){
    curr = input;
    isEnd = false;
    index = -1;
    peek = '\0';
    dim = k;
}

/**
 * isNumber
 */
void TokenStream::readNumber(Token& token) {
    double floatValue = peek - '0';
    for (read(); isdigit(peek); read()) {
        floatValue = floatValue * 10 + peek - '0';
    }

    if (peek == '.') {
        // digit
        double rate = 10;
        for (read(); isdigit(peek); read()) {
            floatValue = floatValue + (peek - '0') / rate;
            rate *= 10;
        }
    }
    token = Token(TokenType::NUMBER, floatValue, std::to_string(floatValue));

    if (peek != 0){
        index--;
    }
    return;
}

/**
 * isLetter
 * only when first char is letter
 */
void TokenStream::readWord(Token& token) {
    std::string word(" ");
    word[0] = peek;
    for (read(); isdigit(peek) || isalpha(peek) || peek == '_'; read()) {
        // can be digit, char or _
        word.push_back(peek);
    }
    if (peek != 0){
        index--;
    }

    // first find function
    auto functionType = TokenStream::Functions.find(word);
    if (functionType != TokenStream::Functions.end()) {
        token = Token(TokenType::FUNCTION, functionType->second, word);
        return;
    }
    // otherwise variable
    auto var = TokenStream::Vars.find(word);
    if (var != TokenStream::Vars.end()){
        if (var->second > dim){
            // TODO EXCEPTTION
            throw ReadTokenException("Expression dimension is larger than setup!\n");
        }
        token = Token(TokenType::VARIABLE, var->second, word);
        return;
    }
}

/**
 * read a char from stream to peek
 */
void TokenStream::read() {
    if (index == int(curr.size())){
        peek = '\0';
        return;
    }
    index++;
    peek = curr[index];
}

int TokenStream::opPriority(TokenType type){
    switch (type){
        case (POW): {
            return 3;
        }
        case (MUL): case (DIV): {
            return 2;
        }
        case (ADD): case (SUB): {
            return 1;
        }
    }
    return 0;
}

bool TokenStream::isLeft(TokenType type){
    return (type != POW);
}

void TokenStream::shunting(std::vector<Token> &nums){
    if (!nums.empty()){
        std::cout<<"Warning: old expression will be cleared"<<std::endl;
        nums.clear();
    }
    std::stack<Token> ops;
    
    // Get tokens
    for (Token token = next(); token.getType() != TokenType::END; token = next()) {
        if (token.getType() == FUNCTION){
            ops.push(token);
        }
        else if (token.getType() == VARIABLE || token.getType() == NUMBER){
            nums.push_back(token);
        }
        else if ((token.getType() == SUB && index == 0) ||  (token.getType() == SUB && index > 0 && curr[index-1] == '(')){
            nums.push_back(Token(NUMBER, 0, "0"));
            ops.push(token);
        }
        else if (token.getType() == OPEN_PAR){
            ops.push(token);
        }
        else if (token.getType() == CLOSE_PAR){
                bool pe = false;
                // All terms between parentheses
                while (!ops.empty())
                {
                    if (ops.top().getType() == OPEN_PAR){
                        pe = true;
                        break;
                    }
                    else{
                        nums.push_back(ops.top());
                        ops.pop();
                    }
                }

                if (!pe){
                    nums.clear();
                    throw ReadTokenException("Parentheses mismatched\n");
                }
                // pop open par 
                ops.pop();

                // if function name
                if (!ops.empty())
                {
                    if (ops.top().getType() == FUNCTION)
                    {
                        nums.push_back(ops.top());
                        ops.pop();
                    }
                }            
        }
        // operator
        else if (token.getType() == ADD|| token.getType() == SUB || token.getType() == POW || token.getType() == DIV || token.getType() == MUL){
            while(!ops.empty() && (
                opPriority(ops.top().getType()) >=  opPriority(token.getType()) && isLeft(token.getType()) || 
                opPriority(ops.top().getType()) > opPriority(token.getType()) && !isLeft(token.getType()))
            ){
                nums.push_back(ops.top());
                ops.pop();
            }
            ops.push(token);
        }
        else{
            throw ReadTokenException("Unknown token type\n");
        }
    }

    while(!ops.empty()){
        if (ops.top().getType() == OPEN_PAR || ops.top().getType() == CLOSE_PAR){
            nums.clear();
            throw ReadTokenException("Parentheses mismatched\n");          
        }
        nums.push_back(ops.top());
        ops.pop();
    }

    #ifdef DEBUG
    for(auto a: nums){
        std::cout<<a<<" ";
    }
    std::cout<<std::endl;
    #endif
}
