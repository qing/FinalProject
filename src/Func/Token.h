#pragma once

#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "Exceptions.h"


/**
 * Enum used to indicate the type of a token
 */
enum TokenType{
    VARIABLE,
    NUMBER,
    FUNCTION,
    ADD,
    SUB,
    MUL,
    DIV,
    POW,
    OPEN_PAR,
    CLOSE_PAR,
    END
};

/**
 * Enum used to indicate the type of a Function
 */
enum FunctionType{
    EXP,
    SUM,
    SQRT,
    LOG,
    COS,
    SIN,
    TAN,
    ATAN
};

/**
 * Enum used to indicate the varibles, which is convenient but not elegent in this task
 */
enum VarType{
    x1 = 1,
    x2,
    x3,
    x4,
    x5,
    x6,
    x7,
    x8,
    x9,
    x10,
    x11,
    x12,
    x13,
    x14,
    x15,
    x16,
    x17,
    x18,
    x19,
    x20,
    x21,
    x22,
    x23,
    x24,
    x25,
    x26,
    x27,
    x28,
    x29,
    x30,
};

/**
 * a class for store the token type, token name and token true value
 */
class Token {
public:
    Token();
    Token(TokenType t);
    Token(TokenType t, char a);
    Token(TokenType t, FunctionType f, std::string& word);
    Token(TokenType t, VarType var, std::string& word);
    // TODO: double to template
    Token(TokenType t, double v, std::string word);
    
    double getValue() const;
    VarType getVar() const;
    FunctionType getFunc() const;

    TokenType getType() const;

    bool isSet = false;
    friend std::ostream &operator<<( std::ostream &output, const Token &D);

private:
    TokenType type;
    FunctionType func;
    VarType var;

    char op;
    double value;
    
    std::string str;
};

/**
 * a class for reading the string, parse the characters to tokens and use shunting algorithm to get a reverse Polish notation of the expression
 */
class TokenStream{
public:
    TokenStream(std::string& input, int k);
    void reset(std::string& input, int k);
    /**
     * Parse the token stream and use shunting algorithm 
     * \param[out] func
     *     The reverse Polish notation 
     */    
    void shunting(std::vector<Token> &v);

private:
    void readNumber(Token& token);
    void readWord(Token& token);
    void read();
    Token next();
    int opPriority(TokenType type);
    bool isLeft(TokenType type);
    bool isEnd = false;
    int index = -1;
    char peek = '\0';
    std::string curr;
    int dim = 0;

    static std::unordered_set<char> BLANK;
    static std::unordered_map<std::string, FunctionType> Functions;
    // Initialized during runtime
    static std::unordered_map<std::string, VarType> Vars;
};

