//
// Created by xiaoqing on 22-11-29.
//
#include "SampleGenerator.h"
#include "inverf.hpp"

template<typename T>
MyGenerator<T>::MyGenerator() = default;
template<typename T>
MyGenerator<T>::~MyGenerator() = default;

template <typename T>
UniformGenerator<T>::UniformGenerator(std::pair<T,T> &interval) {
    this->interval = interval;
}

template <typename T>
T UniformGenerator<T>::operator()() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis_int(std::get<0>(this->interval), std::get<1>(this->interval));
    std::uniform_real_distribution<> dis_real(std::get<0>(this->interval), std::get<1>(this->interval));
    if constexpr(std::is_integral_v<T>) {
        return dis_int(gen);
    }
    return dis_real(gen);
}

template<typename T>
NormalGenerator<T>::NormalGenerator(double mu, double sigma) {
    this->mu = mu;
    this->sigma = sigma;
}

/**
 * params: x
 * return sqrt(-2/(pi*a) - ln(1-x**2)/2 + sqrt(2(/(pi*a)+ln(1-x**2)/2)**2 - 1/a*ln(1-x**2)))
 */
template<typename T>
double NormalGenerator<T>::inverseErrorFunction(double x) {
//    double pi = M_PI;
//    double a = 0.147;
//    double b = log(1-pow(x,2));
//    return sqrt(-2/(pi*a) - b/2 + sqrt(pow(2/(pi*a)+b/2,2) - 1/a*b));
    double t1, t2, lnx, sgn;
    double a = 0.147;
    sgn = (x < 0) ? -1.0 : 1.0;
    double b = (1.0 - x) * (1.0 + x);        // x = 1 - x*x;

    t1 = 2/(M_PI*a) + 0.5 * log(b);
    t2 = 1/(a) * log(b);

    return(sgn*sqrt(-t1 + sqrt(t1*t1 - t2)));
}

template<typename T>
T NormalGenerator<T>::operator()() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis_real(0, 1);
    double z = sqrt(2) * inverseErrorFunction(2*dis_real(gen)-1); // inverf(2*dis_real(gen)-1);
    return mu + z * sigma;

}

template<typename T>
GeneralSampelGenerator<T>::GeneralSampelGenerator(std::string &func_str, T left, T right ,int resolution)
: left(left), right(right), resolution(resolution), uni_dist(0.0, 1.0)
{
    Func func(1, func_str);
    mSampledCDF.resize(resolution + 1);

    Eigen::Matrix<double, Eigen::Dynamic, 1> x0(1);
    x0[0] = left;
    const T cdfLow = func(x0); /// func Eigen::Matrix<double, Eigen::Dynamic, 1> x(3);

    x0[0] = right;
    const T cdfHigh = func(x0);///x[0] = std::stof([left,right]);
    T last_p = 0;
    for (unsigned i = 0; i < mSampledCDF.size(); ++i) {
        const T x = i/resolution*(right - left) + left;
        x0[0] = x;
        const T p = (func(x0) - cdfLow)/(cdfHigh - cdfLow); // normalising
        if (! (p >= last_p)) throw CDFNotMonotonic("CDF Not Monotonic");
        mSampledCDF[i] = Sample{p, x};
        last_p = p;
    }
}

template<typename T>
T GeneralSampelGenerator<T>::operator()()
{

    T cdf = uni_dist(this->gen);
    auto s = std::upper_bound(mSampledCDF.begin(), mSampledCDF.end(), cdf);
    auto bs = s - 1;
    if (bs >= mSampledCDF.begin()) {
        const T r = (cdf - bs->prob)/(s->prob - bs->prob);
        return r*bs->value + (1 - r)*s->value;
    }
    return s->value;
}