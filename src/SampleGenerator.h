//
// Created by xiaoqing on 22-11-26.
//
#pragma once
#ifndef FINALPROJECT_SAMPELGENERATOR_H
#define FINALPROJECT_SAMPELGENERATOR_H

#include <iostream>
#include <vector>
#include <random>
#include <utility>
#include <math.h>
#include "Func.h"
#include "Exceptions.h"
/**
 * parent class for generating samples
 */

template<typename T>
class MyGenerator{
public:
    MyGenerator();
    ~MyGenerator();
    virtual T operator()() = 0;
};

/**
 * generating samples following uniform distribution
 */
template <typename T>
class UniformGenerator : public MyGenerator<T>{
public:
    explicit UniformGenerator(std::pair<T,T> &interval);
    T operator()() override;
private:
    std::pair<T,T> interval;

};

/**
 * generating samples following normal distribution
 */
template <typename T>
class NormalGenerator : public MyGenerator<T>{
public:
    explicit NormalGenerator(double mu, double sigma);
    double inverseErrorFunction(double x);
    T operator()() override;
private:
    double sigma;
    double mu;
};


/**
 * generating samples following a random cdf function distribution
 */
template <typename T>
class GeneralSampelGenerator : public MyGenerator<T>{


public:
    /** \
     *
     * generate the distribution sample of provided function
     *
     * \param[in] func
     *     The user defined function
     * \param[in] left,right
     *     The boundary for the sample interval
     * \param[in] resolution
     *     The precision of the sampling
     */
    GeneralSampelGenerator(std::string &func_str, T left, T right, int resolution);

    /** \
     * return a value sampled by the distribution function
     */
    T operator()() override;
private:

    const T left, right;
    const int resolution;
    std::mt19937 gen;

    struct Sample {
        T prob, value;
        friend bool operator<(T p, const Sample& s) { return p < s.prob; }
    };

    /// total sampled distribution between the bounds
    std::vector<Sample> mSampledCDF;

    /// generator for uniform distribution
    std::uniform_real_distribution<> uni_dist;
};

#endif //FINALPROJECT_SAMPELGENERATOR_H

