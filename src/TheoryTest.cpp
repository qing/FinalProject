#include "TheoryTest.h"

Eigen::MatrixXd genmat(int dim, int count){
    Eigen::MatrixXd mat(dim, count);
    for(int i = 0; i<mat.cols(); i++){
        for(int j = 0; j < mat.rows(); j++){
            mat(j, i) = 2 * rand() / double(RAND_MAX) + 5;
        }
    }
    return mat;
}


TheoryTest::TheoryTest(Calculation* cal): cal(cal){ 
    
}

double TheoryTest::TestCentralLimit(DimensionSampleGenerator<double>* gen, Func& func, int N) {
    std::vector<double> v(N);
    int n;

    for(int i = 0; i<N; i++){
        auto x = gen->getDimensionSample(); // genmat(func.getSize(), 200);
        n = x.cols();

        // Generate n i.i.d samples
        std::vector<double> y(n);
        for(int j = 0; j<n; j++){
            y[j] = func(x.col(j));
            v[i] += y[j];
        }
        v[i] = v[i] / n;
    }

    double mu = cal->CalMoment(v, 1);
    // double sigma2 = (cal->CalMoment(v, 2) - mu * mu) * n;

    // sort for anderson-darling
    std::sort(v.begin(), v.end());
    double Asq = anderson_darling(v, mu);
    return Asq / v.size();    
}