//
// Created by xiaoqing on 22-11-26.
//
#pragma once
#ifndef FINALPROJECT_THEORYTEST_H
#define FINALPROJECT_THEORYTEST_H

#include "Calculation.h"
#include "SampleGenerator.h"
#include "DimensionSampleGenerator.h"
#include "anderson.hpp"

/**
 * a class for testing the central limit theory
 */
class TheoryTest{
public:
    TheoryTest(Calculation* cal);
    /**
     * Calculate the A-square of a given function and a given number of data point
     *
     * \param[in] gen
     *     The random number generator, its dimension is fixed
     * \param[in] func
     *     User defined function
     * \param[in] N
     *     The number of data points used for verify normality
     * \return
     *     A-square statistic / data point size calculated by Anderson-Darling test
     */
    double TestCentralLimit(DimensionSampleGenerator<double>* gen, Func& func, int N = 8192);

private:
    Calculation* cal; ///< Pointer to a class that computes the moment
};

#endif //FINALPROJECT_THEORYTEST_H
