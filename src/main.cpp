//
// Created by xiaoqing on 22-11-25.
//

#include <iostream>
#include "FileReader.cpp"
#include "SampleGenerator.cpp"
#include "DimensionSampleGenerator.cpp"
#include "FileOutput.h"
#include "TheoryTest.h"

using namespace std;

/**
 * this function is used for help judge the input string whether ends with specific characters like .txt or .json
 */
bool hasEnding (std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

/**
 * the main function is a flow chart for helping the user to run this programme
 */
int main(int argc, char *argv[]) {

    /// 1 input the file to read
    std::cout<<" 1 please input the file you want to read, the file should ends with .txt or .json; (recommend (src/)input.txt, (src/)input.json)"<<endl;
    std::string url;
    FileReader<double> *reader;
    cin>>url;
    cin.ignore();
    if(hasEnding(url, ".txt")){
        reader = new TxtReader<double>(url);

    }
    else if(hasEnding (url, ".json")){
        reader = new JsonReader<double>(url);
    }
    else{
        std::cout<<"the file is not ends with .json nor .txt, we use the default file input.json"<<std::endl;
        reader = new JsonReader<double>("input.json");
    }
    reader->readData();
    ///Get the value you want
    auto funcs = reader->getFunc();
    auto interval = reader->geInterval();
    auto sample_num = reader->getSampleNumber();
    auto m = reader->getOrder();
    auto function = reader->getFuncPdf();
    auto mu = reader->getMu();
    auto sigma = reader->getSigma();
    auto dimension = reader->getDimension();
    std::cout<<"Read successfully !!!"<<std::endl;


    /// 2 choose the distribution type
    std::cout<<"2 please input the type of distribution you want to generate random sample (uniform, normal, other) "<<std::endl;
    std::string sample_type;
    std::vector<std::string> distribution_type;
    DimensionSampleGenerator<double> *sample;
    while(true){
        cin>>sample_type;
        cin.ignore();
        if (sample_type == "uniform"){
            distribution_type.emplace_back("Uniform distribution");
            sample = new UniformDimensionGenerator<double>(dimension,interval,sample_num);
            std::cout<<"2.1 the sample number is "<<sample_num<<std::endl;
            std::cout<<"2.2 the dimension of sample is "<<dimension<<std::endl;
            std::cout<<"2.3 the interval of the uniform distribution for each dimension is"<<std::endl;

            for(const auto& it:interval) {
                distribution_type.push_back("[" + to_string(it.first) + ", " + to_string(it.second) + "]");
                std::cout << "[" << it.first << ", " << it.second << "]" << "  ";
            }
            std::cout<<std::endl;
            break;
        }
        else if (sample_type == "normal"){
            distribution_type.emplace_back("Normal distribution");
            distribution_type.emplace_back("mu : ");
            sample = new NormalDimensionSampleGenerator<double>(dimension,mu,sigma,sample_num);
            std::cout<<"2.1 the sample number is "<<sample_num<<std::endl;
            std::cout<<"2.2 the dimension of sample is "<<dimension<<std::endl;
            std::cout<<"2.3 the mu of the normal distribution for each dimension is";
            for(const auto& it:mu) {
                distribution_type.push_back(to_string(it));
                std::cout << it << " "; }
            distribution_type.emplace_back("sigma : ");
            std::cout<<std::endl;
            std::cout<<"2.4 the sigma of the normal distribution for each dimension is";
            for(const auto& it:sigma) {
                distribution_type.push_back(to_string(it));
                std::cout << it << " "; }
            std::cout<<std::endl;
            break;
        }
        else if (sample_type == "other"){
            distribution_type.emplace_back("Self defined distribution");
            distribution_type.emplace_back("Functions : ");
            sample = new GeneralDimensionGenerator<double>(dimension,interval,sample_num,funcs);
            std::cout<<"2.1 the sample number is "<<sample_num<<std::endl;
            std::cout<<"2.2 the dimension of sample is "<<dimension<<std::endl;
            std::cout<<"2.3 the CDF function for each dimension is"<<std::endl;
            for(const auto& it:funcs) {
                distribution_type.push_back(it);
                std::cout << it << "     "; }
            std::cout<<std::endl;
            std::cout<<"2.4 the interval of the uniform distribution for each dimension is"<<std::endl;
            distribution_type.emplace_back("Intervals : ");
            for(const auto& it:interval) {
                distribution_type.push_back("[" + to_string(it.first) + ", " + to_string(it.second) + "]");
                std::cout << "[" << it.first << ", " << it.second << "]" << "  "; }
            std::cout<<std::endl;
            break;
        }
        else{
            std::cout<<"2.0 Sorry, we cannot recognize the input, please reenter"<<std::endl;
        }
    }

    /// 3 choose the calculating order
    /// you can both generate int or double/float, if you want to use int, the former reader should also be <int>
    /// vector<vector> first dimension:sample size, second dimension sample dimension
    auto sample_matrix = sample->getDimensionSample();
    std::cout<<"3 Calculating statistical momentum. "<<std::endl;
    std::cout<<"3.1 The function for calculating statistical momentum is :  "<<function<<std::endl;
    Func func(2, function);
    Calculation* cal = new Calculation();
    int flag=1;
    std::cout<<"input 0 for skip calculating momentum, 1 for continue"<<std::endl;
    while(cin>>flag && flag!=0){
        int m_cal;
        std::cout<<"3.2 choose the order you want to use for calculating the statistical momentum.(0,1,2,...)"<<std::endl;
        cin>>m_cal;cin.ignore(1000, '\n');
        auto sm = cal->CalMoment(func,m_cal,sample_matrix);
        std::cout<<"3.3 statistical momentum for order "<<m_cal<<" is "<<sm<<std::endl;
        std::cout<<"input 0 for break, 1 for use other order"<<std::endl;
    }
    auto sm = cal->CalMoment(func,m,sample_matrix);

    /// 4 verify the central limit theory
    std::cout<<"4 Verify central limit theory. "<<std::endl;
    TheoryTest test(cal);
    std::cout<<"4.1 the calculating function is "<<function<<std::endl;
    std::cout<<"4.2 the similarity error to "<< sample_type<<" distribution is : "<<std::endl;
    if(sample_type=="other") std::cout<<"Warning !!!! the self defined cdf function may not obey the rule of central limit theory"<<std::endl;
    std::cout<<"...waiting..."<<std::endl;
    double sim = test.TestCentralLimit(sample, func);
    std::cout<<sim<<std::endl;


    /// 5 input the output file name
    std::cout<<"5 Output results"<<std::endl;
    std::string outlink;
    std::cout<<" 5.1 please input the file you want to output result, the file should ends with .txt or .json; (recommend output.txt, output.json)"<<endl;
    cin>>outlink;
    std::cout<<"the output file is"<<outlink<<std::endl;
    FileOutput *putter;
    if(hasEnding(outlink, ".txt")){
        putter = new TxtOutput(sim,m,sm,distribution_type , function);
    }
    else if(hasEnding (outlink, ".json")){
        putter = new JsonOutput(sim,m,sm,distribution_type , function);
    }
    else{
        std::cout<<"the file is not ends with .json nor .txt, we use the default file output.json"<<std::endl;
        putter = new JsonOutput(sim, m, sm, distribution_type , function);
        outlink = "output.json";
    }
    putter->outPut(outlink);
    std::cout<<"5.2 Save successfully"<<std::endl;


    /// 6 the end
    std::cout<<"6 Thanks for using"<<std::endl;

    delete cal;

    return 0;
}
