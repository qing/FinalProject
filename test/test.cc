#include <cmath>
#include <gtest/gtest.h>
#include "Func.h"
#include "TheoryTest.h"
#include "SampleGenerator.cpp"
#include "DimensionSampleGenerator.cpp"
#include "FileReader.cpp"

double eps = 0.0001;
double epsMoment = 0.01;
double epsAsq = 0.001;

// INPUT

TEST(INPUT, readfromtxt){
    FileReader<double> *reader = new TxtReader<double>("test.txt");
    reader->readData();
    EXPECT_TRUE(reader->getFuncPdf() == "exp(x1)+log(x2)");
    EXPECT_TRUE(abs(reader->getSigma()[0]) < eps);
}

TEST(INPUT, readfromjson){
    FileReader<double> *reader = new JsonReader<double>("test.json");
    reader->readData();
    EXPECT_TRUE(reader->getSampleNumber() == 2048);
    EXPECT_TRUE(reader->getOrder() == 3);
}

// FUNCTION

TEST(FUNC, sqrt){
    Func func(1, "sqrt(x1)");
    Eigen::VectorXd x(1);
    x(0) = 25;
    double f = func(x);
    EXPECT_TRUE(f >= 5-eps && f <= 5 + eps);
}

TEST(FUNC, explog){
    Func func(2, "exp(x1) + log(x2)");
    Eigen::VectorXd x(2);
    x(0) = 4;
    x(1) = 5;
    double f = func(x);
    EXPECT_TRUE(f >= exp(x(0)) + log(x(1))-eps && f <= exp(x(0)) + log(x(1))+eps);
}

TEST(FUNC, power){
    Func func(1, "x1^4");
    Eigen::VectorXd x(1);
    x(0) = 3;
    double f = func(x);
    EXPECT_TRUE(f >= pow(x(0), 4)-eps && f <= pow(x(0), 4)+eps);
}

TEST(FUNC, arithmetic){
    Func func(5, "x1 + x2 - x3 * x4  + x5 / x3");
    Eigen::VectorXd x(5);
    x(0) = 1;
    x(1) = 10;
    x(2) = 2;
    x(3) = 0.5;
    x(4) = 100;
    double f = func(x);
    EXPECT_TRUE(f >= x(0)+x(1)-x(2)*x(3)+x(4)/x(2)-eps && f <= x(0)+x(1)-x(2)*x(3)+x(4)/x(2)+eps);
}

TEST(FUNC, trigonometric){
    Func func(4, "sin(x1) + cos(x2) + tan(x3) + atan(x4)");
    Eigen::VectorXd x(5);
    x(0) = M_PI_2;
    x(1) = M_PI / 3;
    x(2) = M_PI_4;
    x(3) = 1;
    double f = func(x);
    EXPECT_TRUE(f >= sin(x(0))+cos(x(1))+tan(x(2))+atan(x(3))-eps && f <= sin(x(0))+cos(x(1))+tan(x(2))+atan(x(3))+eps);
}

TEST(FUNC, parenthese){
    Func func(5, "x1 - (x2 - (x3 / (x4 * x5)))");
    Eigen::VectorXd x(5);
    x(0) = 1;
    x(1) = 10;
    x(2) = 2;
    x(3) = 0.5;
    x(4) = 100;
    double f = func(x);
    EXPECT_TRUE(f >= x(0)-(x(1) - (x(2) / (x(3) * x(4))))-eps && f <= x(0)-(x(1) - (x(2) / (x(3) * x(4))))+eps);        
}


// RANDOM

TEST(Random, normal){
    std::vector<double> mu = {0};
    std::vector<double> sd = {1};

    Calculation* cal = new Calculation();
    DimensionSampleGenerator<double>* sampler = new NormalDimensionSampleGenerator<double>(1, mu, sd, 8192);

    Eigen::MatrixXd x = sampler->getDimensionSample();

    std::vector<double> v(8192);
    for(int i = 0; i<8192; i++){
        v[i] = x.col(i)[0];
    }

    std::sort(v.begin(), v.end());

    double similarity = anderson_darling(v) / v.size();
    std::cout<<"Asq/n "<<similarity<<std::endl;
    delete cal;
    delete sampler;
    EXPECT_TRUE(similarity < epsAsq);
}


// CALCULATING MOMENT

TEST(CalculationTest, Moment1order){
    Func func(1, "x1");
    Calculation* cal = new Calculation();
    std::vector<std::pair<double, double>> interval = {{0, 1}};
    DimensionSampleGenerator<double>* sampler = new UniformDimensionGenerator<double>(1, interval, 10000);
    double moment = cal->CalMoment(func, 1, sampler->getDimensionSample());
    // std::cout<<moment<<std::endl;
    EXPECT_TRUE(moment < 0.5+epsMoment && moment > 0.5-epsMoment);
}


TEST(CalculationTest, Moment2order){
    Func func(1, "x1");
    Calculation* cal = new Calculation();
    std::vector<std::pair<double, double>> interval = {{0, 1}};
    DimensionSampleGenerator<double>* sampler = new UniformDimensionGenerator<double>(1, interval, 100000);
    double moment = cal->CalMoment(func, 2, sampler->getDimensionSample());
    // std::cout<<moment<<std::endl;
    EXPECT_TRUE(moment < 1.0/3+epsMoment && moment > 1.0/3-epsMoment);
}

// THEORY TEST


TEST(Theory, CentralLimitUsingNormalDistribution){
    Func func(1, "x1");
    Calculation* cal = new Calculation();
    TheoryTest theo(cal);
    std::vector<double> mu = {5};
    std::vector<double> sd = {10};
    DimensionSampleGenerator<double>* sampler = new NormalDimensionSampleGenerator<double>(1, mu, sd, 100);

    double similarity = theo.TestCentralLimit(sampler, func);
    std::cout<<"Asq/n "<<similarity<<std::endl;
    delete cal;
    delete sampler;
    EXPECT_TRUE(similarity < epsAsq);
}

TEST(Theory, CentralLimitUsingUniformDistribution){
    Func func(1, "x1");
    Calculation* cal = new Calculation();
    TheoryTest theo(cal);
    std::vector<std::pair<double, double>> interval = {{0, 1}};
    DimensionSampleGenerator<double>* sampler = new UniformDimensionGenerator<double>(1, interval, 100);

    double similarity = theo.TestCentralLimit(sampler, func);
    std::cout<<"Asq/n "<<similarity<<std::endl;
    delete cal;
    delete sampler;
    EXPECT_TRUE(similarity <epsAsq);
}

TEST(Theory, CentralLimitUsingExponentialDistribution){
    Func func(1, "-log(1-x1)");
    Calculation* cal = new Calculation();
    TheoryTest theo(cal);
    std::vector<std::pair<double, double>> interval = {{0, 1}};
    DimensionSampleGenerator<double>* sampler = new UniformDimensionGenerator<double>(1, interval, 100);

    double similarity = theo.TestCentralLimit(sampler, func);
    std::cout<<"Asq/n "<<similarity<<std::endl;
    delete cal;
    delete sampler;
    EXPECT_TRUE(similarity < epsAsq);
}

TEST(Theory, CentralLimitUsingVectorialInput1){
    Func func(3, "x1 * x3 + x2 * (1-x3)");
    Calculation* cal = new Calculation();
    TheoryTest theo(cal);
    std::vector<std::pair<double, double>> interval = {{0, 1}, {0, 1}, {0, 1}};
    DimensionSampleGenerator<double>* sampler = new UniformDimensionGenerator<double>(3, interval, 100);

    double similarity = theo.TestCentralLimit(sampler, func);
    std::cout<<"Asq/n "<<similarity<<std::endl;
    delete cal;
    delete sampler;
    EXPECT_TRUE(similarity < epsAsq);
}

TEST(Theory, CentralLimitUsingVectorialInput2){
    Func func(2, "cos(x1) + sin(x2) + exp(x1 + x2)");
    Calculation* cal = new Calculation();
    TheoryTest theo(cal);
    std::vector<std::pair<double, double>> interval = {{0, 1}, {0, 1}};
    DimensionSampleGenerator<double>* sampler = new UniformDimensionGenerator<double>(2, interval, 100);

    double similarity = theo.TestCentralLimit(sampler, func);
    std::cout<<"Asq/n "<<similarity<<std::endl;
    delete cal;
    delete sampler;
    EXPECT_TRUE(similarity < epsAsq);
}
